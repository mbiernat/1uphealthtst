require('dotenv').config();
const express = require('express');
const app = express();
const fetch = require('node-fetch');
const cookie = require('cookie');
const url = require('url');

// Simple proxy server to retreive patiend data, clean it and serve it for client app via api endpoints
class Server {
    #port;
    constructor(port) {
        this.#port = process.env.PORT || port;
    }

    init() {
        app.listen(this.#port, () => console.log(`Listening on port ${this.#port}`));

        app.get('/api/auth/authenticate', (req, res) => {
            this.authenticate()
                .then(payload => {

                    //save to cookie
                    res.cookie('access_token', payload.token.access_token, { maxAge: 8640000 })

                    res.send({
                        payload
                    });
                })
                .catch((err => {
                    res.status(401);
                    res.send({ message: err.message });
                }));
        });
        app.get('/api/auth/provision', (req, res) => {
            const patientId = req.params.id;
            this.provisionUser(req, patientId)
                .then(payload => {
                    res.send({ payload });
                })
                .catch((err => {
                    res.status(401);
                    res.send({ message: err.message });
                }))
        });
        app.get('/add/patients', (req, res) => {
            this.authenticate()
                .then(payload => {
                    let token = payload.token.access_token;
                    res.cookie('access_token', token, { maxAge: 8640000 });
                    res.redirect(this.getProvisionPatientUrl(token));
                })
                .catch((err => {
                    res.send({ message: err.message });
                }));
        });
        app.get('/', (req, res) => {  //TODO: convert this to /callback url
            const queryObject = url.parse(req.url, true).query;

            if (queryObject.success) {
                res.send({ message: 'added patients '});
            }
        });
        app.get('/api/patient/all', (req, res) => {
            this.getPatientList(req)
                .then(this.transformPaitents)
                .then(patients => {
                    res.send({ patients });
                })
                .catch((err => {
                    res.status(401);
                    res.send({ message: err.message });
                }))

        });
        app.get('/api/patient/:id', (req, res) => {
            const patientId = req.params.id;
            this.getPatientEverything(req, patientId)
                .then(this.transformPaitentEverything)
                .then(patientEverything => {
                    res.send({ patientEverything });
                })
                .catch((err => {
                    res.status(401);
                    res.send({ message: err.message });
                }))
        });

    }
    async getPatientEverything(req, patientId) {
        const url = this.getPatientEverythingUrl(patientId);
        let token = this.getTokenFromCookie(req);
        const response = await fetch(url, { method: 'get', headers: { 'authorization': `Bearer ${token}` } });

        const json = await response.json();

        if (json.error) {
            throw new Error(json.error_description);
        }

        return json;
    }
    async getPatientList(req) {
        const url = this.getAllPatientsUrl();
        let token = this.getTokenFromCookie(req);
        const response = await fetch(url, { method: 'get', headers: { 'authorization': `Bearer ${token}` } });
        const json = await response.json();

        if (json.error) {
            throw new Error(json.error_description);
        }

        return json;
    }
    async authenticate() {
        const authCode = await this.getAuthCode();
        const token = await this.getToken(authCode.code);
        return { authCode, token };
    }
    async getAuthCode() {
        const url = this.getAuthCodeUrl();
        const response = await fetch(url, { method: 'post' });
        const json = await response.json();
        return json;
    }
    async getToken(code) {
        const url = this.getTokenUrl(code);
        const response = await fetch(url, { method: 'post' });
        const json = await response.json();
        return json;
    }
    async provisionUser() {
        const url = this.getProvisionUserUrl();
        const response = await fetch(url, { method: 'post' });
        const json = await response.json();
        return json;
    }

    transformPaitentEverything(patientEverything) {
        if (!patientEverything.entry) { return ''; }

        return patientEverything.entry.map((medicalRec) => {
            const { resource } = medicalRec;
            const reactions = resource.reaction ? resource.reaction.map(reaction =>
                (reaction.manifestation &&
                    reaction.manifestation.length > 0 &&
                    reaction.manifestation[0]) ? reaction.manifestation[0].text : '') : [];

            const category = (resource.category && resource.category.text) ? resource.category.text : resource.category;

            return {
                id: resource.id,
                type: resource.type,
                status: resource.status,
                resourceType: resource.resourceType,
                substance: resource.substance ? resource.substance.text : '',
                recordedDate: resource.recordedDate,
                category,
                reactions,
            };
        });
    }
    transformPaitents(patients) {
        return patients.entry.map((patient) => {
            const name = patient.resource?.name.filter(a => a.use === "official").shift();
            return {
                id: patient.resource?.id,
                name: (name) ? name.text : '',
                gender: patient.resource?.gender,
                dateOfBirth: patient.resource?.birthDate
            };
        });
    }

    getTokenFromCookie(req) {
        var cookies = cookie.parse(req.headers.cookie);
        return cookies.access_token;
    }
    getProvisionUserUrl() {
        return `${process.env.PATIENTS_BASE_URL}/user-management/v1/user?app_user_id=${process.env.AUTH_EMAIL}&client_id=${process.env.AUTH_CLIENT_ID}&client_secret=${process.env.AUTH_CLIENT_SECRET}`;
    }
    getProvisionPatientUrl(token) {
        return `${process.env.PATIENTS_BASE_URL}/connect/${process.env.AUTH_APP_ID}?access_token=${token}`;
    }
    getAuthCodeUrl() {
        return `${process.env.PATIENTS_BASE_URL}/user-management/v1/user/auth-code?app_user_id=${process.env.AUTH_EMAIL}&client_id=${process.env.AUTH_CLIENT_ID}&client_secret=${process.env.AUTH_CLIENT_SECRET}`;
    }
    getTokenUrl(authCode) {
        return `${process.env.PATIENTS_BASE_URL}/fhir/oauth2/token?grant_type=authorization_code&client_id=${process.env.AUTH_CLIENT_ID}&client_secret=${process.env.AUTH_CLIENT_SECRET}&code=${authCode}`;
    }
    getAllPatientsUrl(fhirVersion = 'dstu2') {
        return `${process.env.PATIENTS_BASE_URL}/fhir/${fhirVersion}/Patient/`;
    }
    getPatientEverythingUrl(patientId, fhirVersion = 'dstu2') {
        return `${process.env.PATIENTS_BASE_URL}/fhir/${fhirVersion}/Patient/${patientId}/$everything`;
    }

}

var server = new Server(5000);
server.init();