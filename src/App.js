import React, { Component } from "react";
import { CssBaseline } from '@material-ui/core';
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import AppHeader from './components/appHeader';
import Authenticate from "./components/authenticate";
import Patients from "./components/patients";
import NotFound from "./components/notFound";
import PatientDetails from "./components/patientDetails";
import { Switch, Redirect } from "react-router-dom";
import { GuardProvider, GuardedRoute } from 'react-router-guards';
import { getIsLoggedIn } from './loggedInUtils';

const useStyles = makeStyles((theme) => ({
  content: {
    padding: theme.spacing(3),
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(2),
    },
  },
}));

const requireLogin = (to, from, next) => {
  if (to.meta.auth) {
    if (getIsLoggedIn()) {
      next();
    }
    next.redirect('/login');
  } else {
    next();
  }
};

class App extends Component {
  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <CssBaseline />
        <AppHeader />
        <main className={classes.content}>
        <GuardProvider guards={[requireLogin]} loading={Authenticate} error={NotFound}>
          <Switch>
            <GuardedRoute path="/login" exact component={Authenticate} />
            <GuardedRoute path="/patient/:id" exact component={PatientDetails} meta={{ auth: true }} />
            <GuardedRoute path="/patients" exact component={Patients} meta={{ auth: true }} />
            <Redirect from="/" to="/patients" />
            <GuardedRoute path="*" component={NotFound} />
          </Switch>
        </GuardProvider>
        </main>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  patients: state.patients.list,
  classes: useStyles,
});
const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(App);