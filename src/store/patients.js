import { createSlice } from "@reduxjs/toolkit";
import { apiCallBegan } from "./api";

const slice = createSlice({
    name: "patients",
    initialState: {
        all: {
            loading: false,
            list: [],
        },
        details: {
            id: null,
            loading: false,
            data: null
        }
    },
    reducers: {
        // action => action handler
        loadAllPatientsRequested: (patients, action) => {
            patients.all.loading = true;
        },
        loadAllPatientsReceived: (patients, action) => {
            patients.all.loading = false;
            patients.all.list = action.payload.patients;
        },
        loadAllPatientsRequestFailed: (patients, action) => {
            patients.all.loading = false;
            console.log('patientsRequestFailed', action.payload);
        },

        loadPatientRequested: (patients, action) => {
            patients.details.id = action.payload;
            patients.details.loading = true;
        },
        loadPatientReceived: (patients, action) => {
            patients.details.loading = false;
            patients.details.data = action.payload.patientEverything;
        },
        loadPatientRequestFailed: (patients, action) => {
            patients.details.loading = false;
            console.log('loadPatientRequestFailed', action.payload);
        }
    }
});

// Actions
export const {
    loadAllPatientsRequested,
    loadAllPatientsReceived,
    loadAllPatientsRequestFailed,

    loadPatientRequested,
    loadPatientReceived,
    loadPatientRequestFailed,
} = slice.actions;

// Reducers
export default slice.reducer;

// Action creators 
export const loadAllPatients = () => (dispatch, getState) => {
    //TODO implement caching
    dispatch(apiCallBegan({
        url: '/api/patient/all',
        onStart: loadAllPatientsRequested.type,
        onSuccess: loadAllPatientsReceived.type,
        onError: loadAllPatientsRequestFailed.type
    }));
}
export const loadPatient = (patientId) => (dispatch, getState) => {
    dispatch(apiCallBegan({
        url: `/api/patient/${patientId}`,
        onStart: loadPatientRequested.type,
        onSuccess: loadPatientReceived.type,
        onError: loadPatientRequestFailed.type
    }));
}