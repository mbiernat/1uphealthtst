import { combineReducers } from 'redux';
import auth from './auth';
import patients from './patients';


const reducer = combineReducers({
    auth,
    patients
})

export default reducer;