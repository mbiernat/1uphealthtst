import axios from 'axios';
import * as apiActions from "../api";
import { logOut } from '../../loggedInUtils';

const api = ({ dispatch, getState }) => next => async action => {
    if (action.type !== apiActions.apiCallBegan.type) return next(action);

    const { url, method, data, headers, onSuccess, onStart, onError } = action.payload;

    if (onStart) {
        dispatch({ type: onStart });
    }

    next(action);

    try {
        const response = await axios({
            url,
            method,
            data,
            headers
        });

        dispatch(apiActions.apiCallSuccess(response.data));
        if (onSuccess) {
            dispatch({ type: onSuccess, payload: response.data });
        }
    } catch (error) {

        dispatch(apiActions.apiCallFailed(error.message));
        if (onError) {
            dispatch({ type: onError, payload: error.message, status: error.response.status });
        }

        if(error.response.status === 401) {
            logOut();
        }
    }
}

export default api;