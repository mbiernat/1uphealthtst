import { createAction } from "@reduxjs/toolkit";

export const apiCallBegan = createAction('api/callBegin');
export const apiCallSuccess = createAction('api/callSuccess');
export const apiCallFailed = createAction('api/callFailed');