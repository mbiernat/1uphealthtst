import { createSlice } from "@reduxjs/toolkit";
import { apiCallBegan } from "./api";

const slice = createSlice({
    name: "auth",
    initialState: {
        loading: false,
        isAuthenticated: false,
    },
    reducers: {
        // action => action handler
        authenticateRequested: (auth, action) => {
            auth.loading = true;
            auth.isAuthenticated = false;
        },
        authenticateReceived: (auth, action) => {
            console.log('authenticateReceived', action.payload);
            auth.loading = false;
            auth.isAuthenticated = (action.payload.access_token !== "");
        },
        authenticateRequestFailed: (auth, action) => {
            auth.loading = false;
            auth.isAuthenticated = false;
            console.log('addUserRequestFailed', action.payload);
        },
    }
});

// Actions
export const {
    authenticateRequested,
    authenticateReceived,
    authenticateRequestFailed,
} = slice.actions;
// Reducers
export default slice.reducer;

// Action creators 
export const authenticate = () => (dispatch, getState) => {
    dispatch(apiCallBegan({
        url: '/api/auth/authenticate',
        onStart: authenticateRequested.type,
        onSuccess: authenticateReceived.type,
        onError: authenticateRequestFailed.type
    }));
};