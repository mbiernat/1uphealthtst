import React from "react";
import Typography from "@material-ui/core/Typography";

const NotFound = () => {
  return (
    <Typography component="h1" variant="h5">
      Route Not Found ...
    </Typography>
  );
};

export default NotFound;
