import React, { Component } from "react";
import { connect } from "react-redux";
import { loadAllPatients } from "../store/patients";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";
import Loading from "./loading";

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
  },
}));

class Patients extends Component {
  componentDidMount() {
    this.props.loadAllPatients();
  }

  render() {
    const { patients, classes } = this.props;

    if (patients.loading || !patients.list) {
      return <Loading text="Loading patients..." />;
    }

    return (
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell align="right">Gender</TableCell>
              <TableCell align="right">Date Of Birth</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {patients.list.map((patient) => (
              <TableRow key={patient.name}>
                <TableCell component="th" scope="row">
                  <Link to={(location) => `/patient/${patient.id}`}>
                    {patient.name}
                  </Link>
                </TableCell>
                <TableCell align="right">{patient.gender}</TableCell>
                <TableCell align="right">{patient.dateOfBirth}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

const mapStateToProps = (state) => ({
  patients: state.patients.all,
  classes: useStyles,
});
const mapDispatchToProps = (dispatch) => ({
  loadAllPatients: () => dispatch(loadAllPatients()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Patients);
