import React, { Component } from "react";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { loadPatient } from "../store/patients";
import Loading from "./loading";

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
  },
}));

class PatientDetails extends Component {
  componentDidMount() {
    const patientId = this.props.match.params.id;
    this.props.loadPatient(patientId);
  }
  render() {
    const { patient, classes } = this.props;

    if (patient.loading || !patient.data) {
      return <Loading text="Loading patient..." />;
    }

    return (
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Type</TableCell>
              <TableCell align="right">Status</TableCell>
              <TableCell align="right">Resource Type</TableCell>
              <TableCell align="right">Substance</TableCell>
              <TableCell align="right">Category</TableCell>
              <TableCell align="right">Substance</TableCell>
              <TableCell align="right">Recorded Date</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {patient.data.map((record) => (
              <TableRow key={record.id}>
                <TableCell component="th" scope="row">
                  {record.type}
                </TableCell>
                <TableCell align="right">{record.status}</TableCell>
                <TableCell align="right">{record.resourceType}</TableCell>
                <TableCell align="right">{record.substance}</TableCell>
                <TableCell align="right">{record.category}</TableCell>
                <TableCell align="right">{record.substance}</TableCell>
                <TableCell align="right">
                  {new Date(record.recordedDate).toDateString()}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

const mapStateToProps = (state) => ({
  patient: state.patients.details,
  classes: useStyles,
});
const mapDispatchToProps = (dispatch) => ({
  loadPatient: (patientId) => dispatch(loadPatient(patientId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PatientDetails);
