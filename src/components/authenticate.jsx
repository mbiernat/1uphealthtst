import React, { Component } from "react";
import {
  Avatar,
  Button,
  CssBaseline,
  Container,
  Typography,
} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import { makeStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { authenticate } from "../store/auth";
import Loading from "./loading"

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

class Authenticate extends Component {
  render() {
    const { authenticate, classes, auth } = this.props;

    if (auth.loading) {
      return (<Loading text="Authenticating..."/>);
    }

    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Authenticate ...
          </Typography>
          <form className={classes.form} noValidate>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              onClick={async (e) => {
                e.preventDefault();
                authenticate();
                this.props.history.replace("/patients");
              }}
              className={classes.submit}
            >
              Authenticate
            </Button>
          </form>
        </div>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  classes: useStyles,
});
const mapDispatchToProps = (dispatch) => ({
  authenticate: () => dispatch(authenticate()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Authenticate);
